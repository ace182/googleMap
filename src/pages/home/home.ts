import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {  GoogleMaps, GoogleMap, GoogleMapsEvent, Marker, GoogleMapOptions} from '@ionic-native/google-maps';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  map: GoogleMap;

  constructor(public navCtrl: NavController, private googleMaps: GoogleMaps) {}

    ionViewDidLoad(){
      this.loadMap();
    }

    loadMap(){

      let mapOptions: GoogleMapOptions = {
        camera: {
          target: {
            lat: 43.0741904,
            lng: -89.3809802
          },
          zoom: 18,
          tilt: 30
        }
      };

      this.map = GoogleMaps.create('map_canvas', mapOptions);

     
      this.map.one(GoogleMapsEvent.MAP_READY)
      .then(() => {
       
        this.getPosition();
      })
      .catch(error =>{
        console.log(error);
      });

    }

    getPosition(): void{
      this.map.getMyLocation()
      .then(response => {
        this.map.moveCamera({
          target: response.latLng
        });
        this.map.addMarker({
          title: 'My Position',
          icon: 'blue',
          animation: 'DROP',
          position: response.latLng
        });
      })
      .catch(error =>{
        console.log(error);
      });
    }

  }
